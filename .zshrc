getBranch() {
    git rev-parse --abbrev-ref HEAD 2> /dev/null
}

autoload -U colors && colors
setopt PROMPT_SUBST;
PS1="%{$fg[cyan]%}%n%{$reset_color%}@%{$fg[green]%}\$(getBranch) %{$fg[yellow]%}%~ %{$reset_color%}$ "

# some more ls aliases
alias ll='ls -lh'
alias la='ls -lah'
alias l='ls -lh'
export CLICOLOR=1

# pushd and popd aliases
alias d='dirs -v'
alias p='pushd'
alias pu='pushd'
alias po='popd'
alias pu1='pushd +1'
alias pu0='pushd -0'

# git aliases
alias g='git status'
alias t='tig'

# nvm setup
export NVM_DIR="$HOME/.nvm"
[ -s "/opt/homebrew/opt/nvm/nvm.sh" ] && . "/opt/homebrew/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm" ] && . "/opt/homebrew/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/alexkozlov/google-cloud-sdk/path.zsh.inc' ]; then . '/Users/alexkozlov/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/alexkozlov/google-cloud-sdk/completion.zsh.inc' ]; then . '/Users/alexkozlov/google-cloud-sdk/completion.zsh.inc'; fi

# Java SDK
export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk-12.0.2.jdk/Contents/Home/

# Google Cloud Credentials
export GOOGLE_APPLICATION_CREDENTIALS="/Users/alexkozlov/.config/gcloud/application_default_credentials.json"
#export GOOGLE_APPLICATION_CREDENTIALS="/Users/alexkozlov/.config/gcloud/sc-neptune-dev-firebase-admin-sdk.json"
#export GOOGLE_APPLICATION_CREDENTIALS="/Users/alexkozlov/.config/gcloud/sc-neptune-dev-firebase-admin-sdk-2.json"
#export GOOGLE_APPLICATION_CREDENTIALS="/Users/alexkozlov/.config/gcloud/sc-neptune-dev-firebase-admin-sdk-3.json"

export GOOGLE_CLOUD_PROJECT=sc-neptune-dev
#export GOOGLE_CLOUD_PROJECT=sc-neptune-production

# Node.js debugging
#export DEBUG=*,-express:*,-body-parser:*
#export DEBUG=session-reviewer:post-contravention-reviews
#export DEBUG=session-reviewer:rule-recurrence
#export DEBUG=session-reviewer:payment-offenses,session-reviewer:permit-offenses,session-reviewer:parking-session
#export DEBUG=session-reviewer:post-contravention-reviews,session-reviewer:payment-permit-hybrid,session-reviewer:parking-session,session-reviewer:parking-policies
#export DEBUG=session-reviewer:post-contravention-reviews,session-reviewer:parking-session,session-reviewer:parking-policies
#export DEBUG=session-reviewer:*,-session-reviewer:parking-policies,-session-reviewer:offenses-for-rule,-session-reviewer:permit-offenses
#export DEBUG=session-reviewer:post-contravention-reviews,session-reviewer:payment-offenses
export DEBUG=blacklist:post
#export DEBUG=review-assignments:post-review-assignments,review-assignments:compute-review-assignments,review-assignments:get-review-assignments

export NODE_OPTIONS=--max-old-space-size=8192
export GREP_OPTIONS=--color=always

export DOCKER_DEFAULT_PLATFORM=linux/amd64

[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
